let checkBtn, favPet, favDisplay, error, AnimalList;
        checkBtn = document.getElementById("myBtn");
        favPet = document.getElementById("fav-pet");
        favDisplay = document.getElementById("fav-pet-display");
        error = document.getElementById("fav-error");
        AnimalList = ["lion", "dog", "cat", "goat", "rabbit", "horse"]
        favPet.focus();

        const farmValidator = () => {

            if (favPet.value) {
                error.style.display = "none";
                validator();
            } else {
                error.style.display = "block";
            }
            favPet.value = "";
            favPet.focus();
        }

        const validator = () => {
            for (animal of AnimalList) {
                if (animal.toLowerCase() === favPet.value.toLowerCase()) {
                    return favDisplay.innerHTML = "<h1 class=\"animal\">" +
                        favPet.value +
                        "</h1>" +
                        "<p class=\"animal-sub\">Your favorite PET</br>is listed in our animal farm.</p>";
                } else {
                    if (animal === AnimalList[AnimalList.length - 1]) {
                        return favDisplay.innerHTML = "<h1 class=\"animal animal--modified\">Ooops</h1>" +
                            "<p class=\"animal-sub\">No luck</br>please try another PET.</p>";
                    }
                }
            }
        }

        checkBtn.addEventListener("click", farmValidator);
